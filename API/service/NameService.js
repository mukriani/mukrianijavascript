module.exports = exports = (server, pool) => {
    server.post('/api/namepost', (req, res) => {
        console.log(req.body);
        const {last_name,first_name
        } = req.body;
        var query =`INSERT INTO public."m_name"("first_name","last_name","is_delete")			
            VALUES ( '${first_name}','${last_name}',false);`
               pool.query(query,
                (error, result) => {
                    if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: `Data Saved! New name has been add with Name ${first_name} !`
                })
            }
        })
        console.log(query)

    });

    server.post('/api/getname', (req, res) => {
        const { date1, name1, code1, page, pagesize, created} = req.body;
        let perpage = (page -1) * pagesize;
        var query = `SELECT "id","first_name", "last_name" FROM "m_name" where "is_delete"=false
        LIMIT ${pagesize} OFFSET ${perpage};`
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                } else {
                    res.send(200, {
                        success: true,
                        result: result.rows
                    })
                }
            }
        );
        //console.log(query)
    });

    server.post('/api/countname', (req, res) => {
        const { date1, code1, name1, created, page, pagesize} = req.body;       
        var query = `SELECT count(id) as totaldata FROM "m_name" where "is_delete"= 'false';`
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                } else {
                    res.send(200, {
                        success: true,
                        result: result.rows
                    })
                }
            }
        );
        //console.log(query)
    });

    server.put('/api/updatename/:id', (req, res) => {
        const id = req.params.id;
        const {
            first_name, last_name
        } = req.body;

        var query =`UPDATE public."m_name"
        SET 
        "first_name"='${first_name}',
        "last_name"='${last_name}'
        WHERE "id"= ${id};`
        pool.query(query,
            (error, result) => {
                if (error) {
                res.send(400, {
                    success: false,
                    result: error
                });

            } else {
                res.send(200, {
                    success: true,
                    result: `Data Update ! Data name has been update !`
                });

            }

        });
        console.log(query)

    });

    server.get('/api/getnamebyid/:id', (req, res) => {
        const id = req.params.id;
        var query = `SELECT "id", "first_name", "last_name"
        FROM public."m_name" where "id"=${id}`
        pool.query(query,
            (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows[0]
                })
            }
        })
        console.log(query)
    });


    server.put('/api/deletename/:id', (req, res) => {
        const id = req.params.id;
        const { first_name } = req.body;

        var query=`update public."m_name"
        SET "is_delete" = 'true' 
        WHERE "id"='${id}'`
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    });
                } else {
                    res.send(200, {
                        success: true,
                        result: `Data Deleted! Data Name with code ${first_name} has been deleted !`
                    });
                }
            }
        )
        console.log(query)
    });

}