module.exports = exports = (server, pool) => {
    server.post('/api/employeepost', (req, res) => {
        console.log(req.body);
        const {is_delete, created_by, created_date, employee_number, first_name, last_name, m_company_id, email            
        } = req.body;
        var query =`INSERT INTO public."m_employee"("is_delete", "created_by", "created_date", "m_company_id","employee_number", "first_name", "last_name", "email")			
            VALUES ( false, 'Administrator', current_timestamp,'${m_company_id}','${employee_number}','${first_name}', '${last_name}', '${email}');`
               pool.query(query,
                (error, result) => {
                    if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: `Data Saved! New employe has been add with employee ID Number ${employee_number} !`
                })
            }
        })
        //console.log(query)

    });

    server.post('/api/getemployee', (req, res) => {
        const { date1, firstname, name1, code1, page, pagesize, created} = req.body;
        let qFilter = date1 != "" ? ` AND '%${date1}%'  BETWEEN a."created_date" AND a."created_date"  ` : ``;
        let qFilterfirstname = firstname != "" ? `  AND a."first_name" LIKE '%${firstname}%'  ` : ``;
        let qFiltercreated = created != "" ? ` AND a."created_by" LIKE '%${created}%' ` : ``;
        let qFiltercode= code1 !="" ? ` AND a."employee_number" LIKE '%${code1}%'` : ``;
        let qFiltername = name1 != "" ? ` AND b."name" LIKE '%${name1}%' ` : ``;
        let perpage = (page -1) * pagesize;
        var query = `SELECT a."id", a."employee_number", a."first_name",a."last_name",a."m_company_id", 
        TO_CHAR(a."created_date" :: DATE, 'dd/mm/yyyy') as created_date, a."created_by", b."name" FROM "m_company" as b join
        "m_employee" as a on a."m_company_id"=b."id"  
        where a."is_delete"= 'false'
        ${qFilter}  ${qFilterfirstname} ${qFiltercreated} ${qFiltercode} ${qFiltername} LIMIT ${pagesize} OFFSET ${perpage};`
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                } else {
                    res.send(200, {
                        success: true,
                        result: result.rows
                    })
                }
            }

        );
     //console.log(query)
    });

    server.post('/api/countemployee', (req, res) => {
        const { date1, firstname, name1, code1, page, pagesize, created} = req.body;
        let qFilter = date1 != "" ? ` AND '%${date1}%'  BETWEEN a."created_date" AND a."created_date" ` : ``;
        let qFilterfirstname = firstname != "" ? `  AND a."first_name" LIKE '%${firstname}%' ` : ``;
        let qFiltercreated = created != "" ? ` AND a."created_by" LIKE '%${created}%' ` : ``;
        let qFiltercode= code1 !="" ? ` AND a."employee_number" LIKE '%${code1}%'` : ``;
        let qFiltername = name1 != "" ? ` AND b."name" LIKE '%${name1}%' ` : ``;
        let perpage = (page -1) * pagesize;
        var query = `SELECT count(a."id") as totaldata FROM "m_employee" as a join "m_company" as b
        on a."m_company_id"=b."id" where a."is_delete"= 'false'
        ${qFilter} ${qFilterfirstname} ${qFiltercreated} ${qFiltercode} ${qFiltername};`
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                } else {
                    res.send(200, {
                        success: true,
                        result: result.rows
                    })
                }
            }
        );
        //console.log(query)
    });

    server.put('/api/updateemployee/:id', (req, res) => {
        const id = req.params.id;
        const {
            employee_number, first_name, last_name, m_company_id, email 
        } = req.body;

        var query = `UPDATE public."m_employee"
        SET 
        "employee_number"='${employee_number}', 
        "first_name"='${first_name}', 
        "last_name"='${last_name}',
        "m_company_id"='${m_company_id}',
        "email"='${email}',
        "updated_by"='Administrator',
        "updated_date"= current_timestamp
        WHERE "id"= ${id};`
        pool.query(query,
            (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                });

            } else {
                res.send(200, {
                    success: true,
                    result: `Data Update ! Data employee has been update !`
                });

            }

        });
        //console.log(query)
    });

    server.get('/api/getemployeebyid/:id', (req, res) => {
        const id = req.params.id;
        var query = `SELECT a."id", a."employee_number", a."first_name",a."last_name",a."m_company_id", a."email",
        TO_CHAR(a."created_date" :: DATE, 'dd/mm/yyyy') as created_date, a."created_by", b."name" FROM "m_company" as b join
        "m_employee" as a on a."m_company_id"=b."id"  
        where a."id"=${id}`
        pool.query(query,
            (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows[0]
                })
            }
        })
        console.log(query)
    });


    server.put('/api/deleteemployee/:id', (req, res) => {
        const id = req.params.id;
        const { employee_number } = req.body;

        var query = `update public."m_employee" 
        WHERE "id"='${id}'`
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    });
                } else {
                    res.send(200, {
                        success: true,
                        result: `Data Delete! Data employee with Employee ID Number ${employee_number} has been deleted !`
                    });
                }
            }
        )
    });

    server.get('/api/getname', (req, res) => {
        var query = `SELECT*FROM public."m_company" where "is_delete"='false'`
        pool.query(query,
            (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                });
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                });

            }
        })
        //console.log(query)
    });


}