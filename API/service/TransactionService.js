module.exports = exports = (server, pool) => {
    server.post('/api/transactionpost', (req, res) => {
        console.log(req.body);
        const {event_name, start_date, end_date, place, budget,note,code, status,request_by, request_date, created_date, created_by} = req.body;
        var query =`INSERT INTO public."m_transaction"("created_by","created_date","is_delete","status",
        "event_name","place","start_date","end_date",
        "code","request_by", "request_date", "budget", "note")			
            VALUES ( 'Mukriani', current_timestamp,false, 'Submitted',
            '${event_name}','${place}', '${start_date}','${end_date}',
            '${code}','${request_by}', current_timestamp, '${budget}', '${note}');`
               pool.query(query,
                (error, result) => {
                    if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: `Data Saved! Transaction event request has been add with code ${code} !`
                })
            }
        })
         //console.log(query)

    });

    server.post('/api/gettransaction', (req, res) => {
        const { date1, name1, code1, requestby, requestdate, assign,page, pagesize, created} = req.body;
        let qFilter = date1 != "" ? ` AND '%${date1}%'  BETWEEN "created_date" AND "created_date" ` : ``;
        let qFiltercreated = created != "" ? ` AND "created_by" LIKE '%${created}%' ` : ``;
        let qFiltercode= code1 !="" ? ` AND "code" LIKE '%${code1}%'` : ``;
        let qFiltername = name1 != "" ? ` AND "status" LIKE '%${name1}%' ` : ``;
        let qFilterrequestby = requestby != "" ? ` AND "request_by" LIKE '%${requestby}%' ` : ``;
        let qFilterrequestdate = requestdate != "" ? `AND '%${requestdate}%'  BETWEEN "request_date" AND "request_date"'` : ``;
        let perpage = (page -1) * pagesize;
        var query = `SELECT "id", "code", "request_by",TO_CHAR("request_date" :: DATE, 'dd/mm/yyyy') as request_date, TO_CHAR("created_date" :: DATE, 'dd/mm/yyyy') as created_date, "created_by" ,"status" 
        FROM "m_transaction" where "is_delete"= 'false'
        ${qFilter} ${qFiltercreated} ${qFiltercode} ${qFiltername} ${qFilterrequestby} ${qFilterrequestdate} LIMIT ${pagesize} OFFSET ${perpage};`
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                } else {
                    res.send(200, {
                        success: true,
                        result: result.rows
                    })
                }
            }
        );
        //console.log(query)
    });

    server.post('/api/counttransaction', (req, res) => {
        const { date1, code1, name1, requestby,requestdate, created, page, pagesize} = req.body;
        let qFilter = date1 != "" ? ` AND '%${date1}%'  BETWEEN "created_date" AND "created_date" ` : ``;
        let qFiltercreated = created != "" ? ` AND "created_by" LIKE '%${created}%' ` : ``;
        let qFiltercode= code1 !="" ? ` AND "code" LIKE '%${code1}%'` : ``;
        let qFiltername = name1 != "" ? ` AND "name" LIKE '%${name1}%' ` : ``;
        let qFilterrequestby = requestby != "" ? ` AND "request_by" LIKE '%${requestby}%' ` : ``;
        let qFilterrequestdate = requestdate != "" ? `AND '%${requestdate}%'  BETWEEN "request_date" AND "request_date"' ` : ``;
        let perpage = (page -1) * pagesize;
        
        var query = `SELECT count("id") as totaldata FROM "m_transaction" where "is_delete"= 'false'
        ${qFilter} ${qFiltercreated} ${qFiltercode} ${qFiltername} ${qFilterrequestby} ${qFilterrequestdate};`
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                } else {
                    res.send(200, {
                        success: true,
                        result: result.rows
                    })
                }
            }
        );
        //console.log(query)
    });

    server.put('/api/updatetransaction/:id', (req, res) => {
        const id = req.params.id;
        const {event_name, start_date, end_date, place, budget,
        note,code, request_by, request_date
        } = req.body;

        pool.query(`UPDATE public."m_transaction"
        SET 
        "event_name"='${event_name}', 
        "start_date"='${start_date}', 
        "end_date"='${end_date}', 
        "place"='${place}',
        "budget"='${budget}',
        "note"='${note}',
        "code"='${code}',
        "request_by"='${request_by}',
        "request_date"='${request_date}'
        WHERE "id"= ${id};`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                });

            } else {
                res.send(200, {
                    success: true,
                    result: `Data ${code} Berhasil diubah`
                });

            }

        });

    });

    server.get('/api/gettransactionbyid/:id', (req, res) => {
        const id = req.params.id;
    var query=`SELECT  "id", "event_name","place",TO_CHAR("start_date" :: DATE, 'dd/mm/yyyy') as start_date,TO_CHAR("end_date" :: DATE, 'dd/mm/yyyy') as end_date,
        "code","request_by", TO_CHAR("request_date" :: DATE, 'dd/mm/yyyy') as request_date, "budget", "note","status", "assign_to"
        FROM public."m_transaction" where "id"=${id}` 
        pool.query(query,
            (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows[0]
                })
            }
        })
console.log(query)
    });


    server.put('/api/deletetransaction/:id', (req, res) => {
        const id = req.params.id;
        const { code } = req.body;

        pool.query(`update public."m_transaction" 
        WHERE "id"='${id}'`,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    });
                } else {
                    res.send(200, {
                        success: true,
                        result: `Data ${code} berhasil dihapus`
                    });
                }
            }
        )
    });
    
    server.put('/api/approve/:id', (req,res) => {

        const id = req.params.id;
        const{code, assign_to} = req.body;
        var query = `update public.m_transaction set "assign_to"='2', "status" = 'In Progress', "updated_by" = 'Administrator', "updated_date" = current_timestamp where "id" = 
        '${id}';`; 
        pool.query(query, (error, result) => {
                if(error){
                    res.send(400,{
                        success:false,
                        result: error
                    })
                }else{
                    res.send(200,{
                        success: true,
                        result: `Izin karyawan ${code} berhasil disetujui`
                    })
                }
            }
        )
        console.log(query);
    });

    server.put('/api/reject/:id', (req,res) => {

        const id = req.params.id;
        const{employeeName, assign_to,reject_reason} = req.body;
        var query = `update public.m_transaction set "assign_to"=''0, "status" = 'Rejected', "updated_by" = Administrator, "updated_date" = current_timestamp where "id" = 
        '${id}';`; 
        pool.query(query, (error, result) => {
                if(error){
                    res.send(400,{
                        success:false,
                        result: error
                    })
                }else{
                    res.send(200,{
                        success: true,
                        result: `Izin karyawan ${employeeName} berhasil disetujui`
                    })
                }
            }
        )
        console.log(query);
    });
    server.get('/api/getuser', (req, res) => {
        var query = `SELECT a."m_role_id", a."m_employee_id",b."name", c."first_name" 
        FROM public."m_user" as a join "m_role" as b
        on a."m_role_id"=b."id"
        join "m_employee" as c
        on a."m_employee_id"=c."id" 
        where b."name"='Staff'`
        pool.query(query,
            (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                });
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows
                });

            }
        })
       // console.log(query)
    });



}