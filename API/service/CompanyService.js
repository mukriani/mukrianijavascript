module.exports = exports = (server, pool) => {
    server.post('/api/companypost', (req, res) => {
        console.log(req.body);
        const {code,email, phone, name, address
        } = req.body;
        var query =`INSERT INTO public."m_company"("created_by","created_date","is_delete","code","email", "phone", "name", "address")			
            VALUES ( 'Administrator', current_timestamp,false, '${code}','${email}', '${phone}', '${name}', '${address}');`
               pool.query(query,
                (error, result) => {
                    if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: `Data Saved! New company has been add with code ${code} !`
                })
            }
        })
        // console.log(query)

    });

    server.post('/api/getcompany', (req, res) => {
        const { date1, name1, code1, page, pagesize, created} = req.body;
        let qFilter = date1 != "" ? ` AND '%${date1}%'  BETWEEN "created_date" AND "created_date" ` : ``;
        let qFiltercreated = created != "" ? ` AND "created_by" LIKE '%${created}%'` : ``;
        let qFiltercode= code1 !="" ? ` AND "code" LIKE '%${code1}%'` : ``;
        let qFiltername = name1 != "" ? ` AND "name" LIKE '%${name1}%' ` : ``;
        let perpage = (page -1) * pagesize;
        var query = `SELECT "id", "code", "name",TO_CHAR("created_date" :: DATE, 'dd/mm/yyyy') as created_date, "created_by" FROM "m_company" where "is_delete"=false
        ${qFilter} ${qFiltercreated} ${qFiltercode} ${qFiltername} LIMIT ${pagesize} OFFSET ${perpage};`
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                } else {
                    res.send(200, {
                        success: true,
                        result: result.rows
                    })
                }
            }
        );
        //console.log(query)
    });

    server.post('/api/countcompany', (req, res) => {
        const { date1, code1, name1, created, page, pagesize} = req.body;
        let qFilter = date1 != "" ? `  AND '%${date1}%'  BETWEEN "created_date" AND "created_date" ` : ``;
        let qFiltercreated = created != "" ? ` AND "created_by" LIKE '%${created}%' ` : ``;
        let qFiltercode= code1 !="" ? ` AND "code" LIKE '%${code1}%'` : ``;
        let qFiltername = name1 != "" ? ` AND "name" LIKE '%${name1}%' ` : ``;
        let perpage = (page -1) * pagesize;
        
        var query = `SELECT count(id) as totaldata FROM "m_company" where "is_delete"= 'false'
        ${qFilter} ${qFiltercreated} ${qFiltercode} ${qFiltername};`
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                } else {
                    res.send(200, {
                        success: true,
                        result: result.rows
                    })
                }
            }
        );
        //console.log(query)
    });

    server.put('/api/updatecompany/:id', (req, res) => {
        const id = req.params.id;
        const {
            code, email, phone, name, address
        } = req.body;

        var query =`UPDATE public."m_company"
        SET 
        "code"='${code}',
        "email"='${email}', 
        "phone"='${phone}', 
        "name"='${name}', 
        "address"='${address}',
        "updated_by"= 'Administrator',
        "updated_date"= current_timestamp
        WHERE "id"= ${id};`
        pool.query(query,
            (error, result) => {
                if (error) {
                res.send(400, {
                    success: false,
                    result: error
                });

            } else {
                res.send(200, {
                    success: true,
                    result: `Data Update ! Data company has been update !`
                });

            }

        });
        //console.log(query)

    });

    server.get('/api/getcompanybyid/:id', (req, res) => {
        const id = req.params.id;
        pool.query(`SELECT "id", "code", "email", "phone", "name", "address"
        FROM public."m_company" where "id"=${id}`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows[0]
                })
            }
        })

    });


    server.put('/api/deletecompany/:id', (req, res) => {
        const id = req.params.id;
        const { name } = req.body;

        var query=`update public."m_company"
        SET "is_delete" = 'true' 
        WHERE "id"='${id}'`
        pool.query(query,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    });
                } else {
                    res.send(200, {
                        success: true,
                        result: `Data Deleted! Data Company with code ${name} has been deleted !`
                    });
                }
            }
        )
        console.log(query)
    });

}