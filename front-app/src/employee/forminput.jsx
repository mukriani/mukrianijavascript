import React from 'react';
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import Select from 'react-select';
import Modal from 'react-bootstrap/Modal';

class FormInput extends React.Component {
    constructor() {
        super();
        this.state = {
            selectedOption: {}
        }
    }
    render() {
        const {EmployeeModel, selectedHandler, mode, NameOption, changeHandler, cancelHandle, cancelHandleCreate, onSave, openmodalEdit, openmodalCreate, handleCancel, errors} = this.props;      
        let options = NameOption.map(function (item) {
            return { value: item.id, label: item.name }
        });
        return (
            <div>
                 <Modal show={openmodalCreate} style={{opacity:3}} class="modal-lg">
                    <Modal.Header style={{ backgroundColor: "#1E90FF" }} >
                        <Modal.Title style={{backgroundColor: "#1E90FF" }}><h4><font color="white">Add Company</font></h4></Modal.Title>
                        {/* {JSON.stringify(ResourceModel)} */}
                    </Modal.Header>
                    <Modal.Body style={{backgroundColor: "white" }}>
                        <div className='form-inside-input'>
                            <div class="col-sm-6">
                            <label className='form-label'> *Emp ID Number</label>&nbsp;<input type="text" id="employee_number" className="form-control"
                            value={EmployeeModel.employee_number} onChange={changeHandler("employee_number")}/>
                            <span style={{color: "red"}}>{errors["employee_number"]}</span><br/>
                            <label className='form-label'> *First Name</label>
                            <input type="text" id="first_name" className="form-control" 
                            value={EmployeeModel.first_name} onChange={changeHandler("first_name")}/>
                            <span style={{color: "red"}}>{errors["first_name"]}</span><br/>
                            <label className='form-label'> Last Name</label>
                            <input type="text" id="last_name" className="form-control"  
                            value={EmployeeModel.last_name} onChange={changeHandler("last_name")}/>
                            </div>
                            <div class="col-sm-6">
                            <label className='form-label'>*Company Name</label>
                            <Select
                                name="m_company_id"
                                value={mode == 'create' ? this.state.value : { label: EmployeeModel.name, value: EmployeeModel.m_company_id}}
                                onChange={selectedHandler}
                                options={options}
                                defaultValue={{ label: EmployeeModel.name, value: EmployeeModel.m_company_id}}
                                placeholder="-Select Company Name-"
                            />
                            <span style={{color: "red"}}>{errors["name"]}</span><br/>
                            <label className='form-label'> Email</label>
                            <input type="text" id="email"  className="form-control" 
                            value={EmployeeModel.email} onChange={changeHandler("email")}/>
                            <span style={{color: "red"}}>{errors["email"]}</span>
                           </div>
                        </div>   
                    </Modal.Body>
                    <Modal.Footer style={{backgroundColor: "white" }}>
                        <div className="modal-footer">
                        <button style={{backgroundColor:"#1E90FF", borderColor:"#1E90FF"}} onClick={onSave}><font color="white">Save</font></button>
                        <button style={{backgroundColor:'orange', borderColor:'orange'}} onClick={cancelHandleCreate}><font color="white">Cancel</font></button>
                        </div>
                    </Modal.Footer>
                </Modal>  
            
                <Modal show={openmodalEdit} style={{opacity:3}}>
                    <Modal.Header style={{ backgroundColor: "#1E90FF" }}>
                        <Modal.Title style={{backgroundColor: "#1E90FF"}}><h4><font color="white">Edit Company - PT. Xsis Mitra Utama ({EmployeeModel.code})</font></h4></Modal.Title>
                        {/* {JSON.stringify(ResourceModel)} */}
                    </Modal.Header>
                    <Modal.Body style={{backgroundColor: "white"}}>
                    <div className='form-inside-input'>
                            <div class="col-sm-6">
                            <label className='form-label'> *Emp ID Number</label>&nbsp;<input type="text" id="employee_number" className="form-control"
                            value={EmployeeModel.employee_number} onChange={changeHandler("employee_number")}/>
                            <span style={{color: "red"}}>{errors["employee_number"]}</span><br/>
                            <label className='form-label'> *First Name</label>
                            <input type="text" id="first_name" className="form-control" 
                            value={EmployeeModel.first_name} onChange={changeHandler("first_name")}/>
                            <span style={{color: "red"}}>{errors["first_name"]}</span><br/>
                            <label className='form-label'> Last Name</label>
                            <input type="text" id="last_name" className="form-control"  
                            value={EmployeeModel.last_name} onChange={changeHandler("last_name")}/>
                            </div>
                            <div class="col-sm-6">
                            <label className='form-label'>*Company Name</label>
                            <Select
                                name="m_company_id"
                                value={mode == 'create' ? this.state.value : { label: EmployeeModel.name, value: EmployeeModel.m_company_id}}
                                onChange={selectedHandler}
                                options={options}
                                defaultValue={{ label: EmployeeModel.name, value: EmployeeModel.m_company_id}}
                                placeholder="-Select Company Name-"
                            />
                            <span style={{color: "red"}}>{errors["name"]}</span><br/>
                            <label className='form-label'> Email</label>
                            <input type="text" id="email"  className="form-control" 
                            value={EmployeeModel.email} onChange={changeHandler("email")}/>
                            <span style={{color: "red"}}>{errors["email"]}</span>
                           </div>
                        </div>  
                    </Modal.Body>
                    <Modal.Footer style={{backgroundColor: "white" }}>
                        <div className="modal-footer">
                        <button style={{backgroundColor:"#1E90FF", borderColor:"#1E90FF"}} onClick={onSave}><font color="white">Update</font></button>
                        <button style={{backgroundColor:'orange', borderColor:'orange'}} onClick={cancelHandle}><font color="white">Cancel</font></button>
                        </div>
                    </Modal.Footer>
                </Modal>  
            </div>
        )
    }
}   
export default FormInput;