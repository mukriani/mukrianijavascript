import axios from 'axios';
import {config} from '../config/config';

const EmployeeService = {
    getAllemployee: (filter) => {
        const result = axios.post(config.apiUrl + '/getemployee', filter)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
    },

    post: (item) => {
        const result = axios.post(config.apiUrl + '/employeepost', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },
    updateemployee: (item) => {
        const result = axios.put(config.apiUrl + '/updateemployee/' + item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    getdetailbyid:(id) => {
        const result = axios.get(config.apiUrl+'/getemployeebyid/'+id)
        .then(response =>{
            return{
                success: response.data.success,
                result: response.data.result
            }
        })
        .catch(error =>{
            return{
                success: false,
                result: error
            }
        });
        return result;
    },

    delete: (item) => {
        const result = axios.put(config.apiUrl + '/deleteemployee/'+item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;

    },
    
    getdatabyid: (id) => {
        const result = axios.get(config.apiUrl + '/getemployeebyid/'+ id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error

                }
            });
        return result;
    },

    countemployee:(filter) => {
        const result = axios.post(config.apiUrl+'/countemployee', filter)
        .then(response =>{
            return{
                success: response.data.success, 
                result: response.data.result
            }
        })
        .catch(error =>{
            return{
                success: false,
                result: error
            }
        });
        return result;
    },
    
    getname:() => {
        const result = axios.get(config.apiUrl+'/getname')
        .then(response =>{
            return{
                success: response.data.success,
                result: response.data.result
            }
        })
        .catch(error =>{
            return{
                success: false,
                result: error
            }
        });
        console.log(result);
        return result;
    }
}
export default EmployeeService;

