import axios from 'axios';
import {config} from '../config/config';

const CompanyService = {
    getAllcompany: (filter) => {
        const result = axios.post(config.apiUrl + '/getcompany', filter)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
    },

    post: (item) => {
        const result = axios.post(config.apiUrl + '/companypost', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },
    updatecompany: (item) => {
        const result = axios.put(config.apiUrl + '/updatecompany/' + item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    getdetailbyid:(id) => {
        const result = axios.get(config.apiUrl+'/getcompanybyid/'+id)
        .then(response =>{
            return{
                success: response.data.success,
                result: response.data.result
            }
        })
        .catch(error =>{
            return{
                success: false,
                result: error
            }
        });
        return result;
    },

    delete: (item) => {
        const result = axios.put(config.apiUrl + '/deletecompany/'+item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;

    },
    
    getdatabyid: (id) => {
        const result = axios.get(config.apiUrl + '/getcompanybyid/'+ id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error

                }
            });
        return result;
    },

    countcompany:(filter) => {
        const result = axios.post(config.apiUrl+'/countcompany', filter)
        .then(response =>{
            return{
                success: response.data.success, 
                result: response.data.result
            }
        })
        .catch(error =>{
            return{
                success: false,
                result: error
            }
        });
        return result;
    },
    
}
export default CompanyService;

