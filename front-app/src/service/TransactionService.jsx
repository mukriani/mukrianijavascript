import axios from 'axios';
import {config} from '../config/config';

const TransactionService = {
    getAlltransaction: (filter) => {
        const result = axios.post(config.apiUrl + '/gettransaction', filter)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
    },

    post: (item) => {
        const result = axios.post(config.apiUrl + '/transactionpost', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },
    updatetransaction: (item) => {
        const result = axios.put(config.apiUrl + '/updatetransaction/' + item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },


    approve: (item) => {
        const result = axios.put(config.apiUrl + '/approve/' + item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    reject: (item) => {
        const result = axios.put(config.apiUrl + '/reject/' + item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    getdetailbyid:(id) => {
        const result = axios.get(config.apiUrl+'/gettransactionbyid/'+id)
        .then(response =>{
            return{
                success: response.data.success,
                result: response.data.result
            }
        })
        .catch(error =>{
            return{
                success: false,
                result: error
            }
        });
        return result;
    },

    delete: (item) => {
        const result = axios.put(config.apiUrl + '/deletetransaction/'+item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;

    },
    
    getdatabyid: (id) => {
        const result = axios.get(config.apiUrl + '/gettransactionbyid/'+ id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error

                }
            });
        return result;
    },

    counttransaction:(filter) => {
        const result = axios.post(config.apiUrl+'/counttransaction', filter)
        .then(response =>{
            return{
                success: response.data.success, 
                result: response.data.result
            }
        })
        .catch(error =>{
            return{
                success: false,
                result: error
            }
        });
        return result;
    },
    
    getuser:() => {
        const result = axios.get(config.apiUrl+'/getuser')
        .then(response =>{
            return{
                success: response.data.success,
                result: response.data.result
            }
        })
        .catch(error =>{
            return{
                success: false,
                result: error
            }
        });
        console.log(result);
        return result;
    }
}
export default TransactionService;

