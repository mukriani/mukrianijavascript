import React from 'react';
import { Link } from 'react-router-dom';

export default class sidebar extends React.Component {
    render() {
        return (

            <aside class="main-sidebar">
                <section class="sidebar">
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="active treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Makrom Aplication</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="company"><i class="fa fa-circle-o"></i>Company</a></li>
                                <li><a href="employee"><i class="fa fa-circle-o"></i>Employee</a></li>
                                <li><a href="transaction"><i class="fa fa-circle-o"></i>Transaction</a></li>
                                <li><a href="name"><i class="fa fa-circle-o"></i>Name</a></li>
                            </ul>   
                        </li>                       
                    </ul>
                </section>
            </aside>
        )
    }
}