import React from 'react';
import company from '../company';
import employee from '../employee';
import transaction from '../transaction';
import name from '../name';
import Header from '../Layout/Header';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import Sidebar from '../Layout/sidebar';


export default class content extends React.Component{
    render(){
        return(
            <div className="wrapper">
                <Sidebar/>
                <div className="content-wrapper">
                    <section className="content">
                        <Switch>
                            <Route exact path='/company' component={company}/>
                            <Route exact path='/employee' component={employee}/>
                            <Route exact path='/transaction' component={transaction}/>
                            <Route exact path='/name' component={name}/>
                        </Switch>
                    </section>
                </div>
            </div>
        )
    }
}