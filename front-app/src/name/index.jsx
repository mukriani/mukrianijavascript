import React from 'react';
import FormInput from './forminput';
import Table from 'react-bootstrap/Table';
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import Modal from 'react-bootstrap/Modal';
import NameService from '../service/NameService';
import {ModalBody,ModalFooter,ModalHeader,Button, Container, Row, Col,Card, CardTitle, CardText, Collapse } from 'reactstrap'
import Dropdown from 'react-bootstrap/Dropdown'

class Name extends React.Component {
    NameModel = {
        id:0,
        is_delete:false, 
        first_name:"", 
        last_name:"", 
                   
    }

    constructor() {
        super();
        this.state = {
            ListName: [],
            NameModel: this.NameModel,
            openmodalEdit: false,
            openmodalDetail: false,
            openmodalDelete: false,
            openmodalCreate: false,
            hidden: true,
            errors: {},
            filter: {
                orderdate1: '',
                page: '1',
                pagesize: '10'
            },
            totaldata:1
        }
    }
   
    componentDidMount() {
        const {filter} = this.state;
        this.loadList(filter);

    }


    loadList = async (filter) => {
        const respon = await NameService.getAllname(filter);
        const countdata = await NameService.countname(filter);
        if (respon.success) {
            this.setState({
                ListName: respon.result,
                ListTampil:respon.result,
                totaldata: Math.ceil(countdata.result[0].totaldata / filter.pagesize)
            })
        }
    }

    componentDidUpdate() {
        const {filter}  = this.state;
        this.loadList(filter);
    }


    handleOpen = () => {
        this.setState({
            hidden: true,
            openmodalCreate: true,
            NameModel: this.NameModel,
            mode: 'create'
        })
    }

    cancelHandleCreate = async () => {
        this.setState({
            openmodalCreate: false,
            errors: {}
        });
    }


    cancelHandle = async () => {
        this.setState({
            openmodalEdit: false,
            errors: {}
        });
    }

    handleEdit = async (id) => {
        const respon = await NameService.getdatabyid(id);
        if (respon.success) {
            this.setState({
                hidden: false,
                openmodalEdit: true,
                NameModel: respon.result,
                mode: "edit"
            })
        }
        else {
            alert('error' + respon.result);
        }
    }


    cancelHandleEdit = async () => {
        this.setState({
            openmodalDelete: false,
            errors: {}

        });

    }

    cancelHandleDelete = async () => {
        this.setState({
            openmodalDelete: false

        });

    }

  

    onSave = async () => {
        const { NameModel, filter, mode } = this.state;
            if (mode === 'create') {
                const respons = await NameService.post(NameModel);
                if (respons.success) {
                    alert('Success : ' + respons.result)
                    this.loadList(filter);
                }
                else {
                    alert('Error : ' + respons.result)
                }
                this.setState({
                    hidden: true,
                    openmodalCreate: false
                });
            } else {
                const respon = await NameService.updatename(NameModel);
                if (respon.success) {
                    alert('Success : ' + respon.result)
                    this.loadList(filter);
                }
                else {
                    alert('Error : ' + respon.result)
                }
                this.setState({
                    hidden: true,
                    openmodalEdit: false
                });
            }
        } 


    handleDelete = async (id) => {
        const respon = await NameService.getdatabyid(id);
        if (respon.success) {
            this.setState({
                hidden:false,
                openmodalDelete: true,
                NameModel: respon.result

            });
        }
        else {
            alert('error' + respon.result);
        }
    }

    sureDelete = async (item) => {
        const { NameModel, filter } = this.state;
        const respons = await NameService.delete(NameModel);
        if (respons.success) {
            alert('Success : ' + respons.result)
            this.loadList(filter);
        }
        else {
            alert('Error : ' + respons.result)
        }
        this.setState({
            openmodalDelete: false
        });
    }

    changeHandler = name => ({ target: { value } }) => {
        this.setState({
        NameModel: {
                ...this.state.NameModel,
                [name]: value,
            }
        })
    }
  
    handleDetail = async (id) => {
        const respon = await NameService.getdetailbyid(id);
        console.log(respon)
        if (respon.success) {
            this.setState({
                hidden:false,
                NameModel:respon.result,
                openmodalDetail: true,
            })
        }
    else {
        alert('error' + respon.result);
    }
}

    handleCancelDetail = async () => {
        this.setState({
            openmodalDetail: false
        })
    }

  
    filterHandler = val => ({ target: { value } }) => {
        this.setState({
            filter: {
                ...this.state.filter,
                [val]: value
            }
        })
    }

    onChangePage = (number) => {
        const { filter } = this.state;
        this.setState({
            filter: {
                ...this.state.filter,
                ["page"]: number
            }
        });
    }

    handlerSorting = () => {
        let orderdate1 = "";
        const { filter } = this.state;
        if (filter.orderdate1 === "") {
            orderdate1 = "DESC"
        }
        this.setState({
            filter: {
                ...this.state.filter,
                ["orderdate1"]: orderdate1
            }
        })
    }


    renderPagination() {
        let items = [];
        const { filter, totaldata } = this.state;
        for (let number = 1; number <= totaldata; number++) {
            items.push(
                <PaginationItem key={number} active={number === filter.page}>
                    <PaginationLink onClick={() => this.onChangePage(number)} next>
                        {number}
                    </PaginationLink>
                </PaginationItem>
            );
        }
        return (
            <Pagination>{items}</Pagination>
        );
    }

    pageSizeHandler = (val) => {
        this.setState({
            filter: {
                ...this.state.filter,
                ["pagesize"]: val
            }
        });
    }


    render() {
        const { ListName, NameModel, hidden, openmodalEdit, openmodalCreate, filter, errors, mode, openmodalDelete, handleCancelDetail,
            handleDetail, openmodalDetail } = this.state;
        return (

            <div>                
                <FormInput
                    ListName={ListName}
                    errors={errors}
                    openmodalCreate={openmodalCreate}
                    openmodalDetail={openmodalDetail}
                    handleDetail={this.handleDetail}
                    NameModel={NameModel}
                    hidden={hidden}
                    title={mode}
                    handleCancelDetail={this.handleCancelDetail}
                    openmodalEdit={openmodalEdit}
                    handleOpen={this.handleOpen}
                    openmodalDelete={openmodalDelete}
                    mode={mode}
                    changeHandler={this.changeHandler}
                    cancelHandleCreate={this.cancelHandleCreate}
                    cancelHandle={this.cancelHandle}
                    onSave={this.onSave}
                    sureDelete={this.sureDelete}
                />
                 <div >
                     <div style={{backgroundColor:"#1E90FF", fontSize:'15px'}} class="card-body"><font color="white">List Name</font></div>
                 </div><br/>
                 <CardText style={{color:'#1E90FF', fontStyle:'Roboto'}}>
                 <h5>
                  
                <button type="button" style={{float:'right'}} class="btn btn-success btn-lg" onClick={() => this.handleOpen()}>Add</button><br/><br/>
                <table class='table table-striped table-xl table-hover' >
                    <thead>
                        <tr style={{borderBottomStyle:'ridge',border:'1',borderBottomColor:'#000066',borderBottomWidth:'2px',textAlign:'-moz-ridge',width:'90%'}}>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            ListName.map(co => {
                                return (
                                    <tr  >
                                        <td>{co.first_name}</td>
                                        <td>{co.last_name}</td>
                                        <td> <button type="button" style={{float:'left'}} class="btn btn-warning btn-lg" onClick={() => this.handleEdit(co.id)}>Edit</button></td>
                                        <td> <button type="button" style={{float:'left'}} class="btn btn-danger btn-lg" onClick={() => this.handleDelete(co.id)}>Delete</button></td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                {this.renderPagination()}
                <Modal show={openmodalDelete} style={{ opacity: 1 }}>
                <Modal.Header class="glyphicon glyphicon-remove" >
                <Modal.Title  ></Modal.Title></Modal.Header>
                    <Modal.Body> 
                    <div > <h5><font color ="black">Delete Data ?</font></h5></div></Modal.Body>
                    <Modal.Footer>
                        <div className="modal-footer">
                        <button style={{backgroundColor:"#1E90FF", borderColor:"blue", color:'white'}} onClick={() => this.sureDelete()}>&nbsp;Delete&nbsp;</button>
                        <button style={{backgroundColor:'orange', borderColor:'orange', color:'white'}} onClick={() => this.cancelHandleDelete()}>Cancel</button>
                        </div>
                        </Modal.Footer>
                </Modal>
                </h5>
                </CardText>
            </div>
        )
    }
}

export default Name;


