import React from 'react';
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import Select from 'react-select';
import Modal from 'react-bootstrap/Modal';

class FormInput extends React.Component {
    constructor() {
        super();
        this.state = {
             }
    }
    render() {
        const {NameModel, mode, changeHandler, cancelHandle, cancelHandleCreate,onSave, openmodalEdit, openmodalCreate, handleCancel} = this.props;      
        return (
            <div>
                 <Modal show={openmodalCreate} style={{opacity:3}} class="modal-lg">
                    <Modal.Header style={{ backgroundColor: "#1E90FF" }} >
                        <Modal.Title style={{backgroundColor: "#1E90FF" }}><h4><font color="white">Add Name</font></h4></Modal.Title>
                        {/* {JSON.stringify(ResourceModel)} */}
                    </Modal.Header>
                    <Modal.Body style={{backgroundColor: "white" }}>
                        <div className='form-inside-input'>
                            <div class="col-sm-12">
                            <label className='form-label'> First Name</label>
                            <input type="text" id="firs_name" className="form-control"
                            value={NameModel.first_name} onChange={changeHandler("first_name")}/><br/>
                            <label className='form-label'> Last Name</label>
                            <input type="text" id="last_name" className="form-control" 
                            value={NameModel.last_name} onChange={changeHandler("last_name")}/><br/>
                            </div>
                            </div>   
                    </Modal.Body>
                    <Modal.Footer style={{backgroundColor: "white" }}>
                        <div className="modal-footer">
                        <button style={{backgroundColor:"#1E90FF", borderColor:"#1E90FF"}} onClick={onSave}><font color="white">Save</font></button>
                        <button style={{backgroundColor:'orange', borderColor:'orange'}} onClick={cancelHandleCreate}><font color="white">Cancel</font></button>
                        </div>
                    </Modal.Footer>
                </Modal>  
            
                <Modal show={openmodalEdit} style={{opacity:3}}>
                    <Modal.Header style={{ backgroundColor: "#1E90FF" }}>
                        <Modal.Title style={{backgroundColor: "#1E90FF"}}><h4><font color="white">Edit Name ({NameModel.first_name})</font></h4></Modal.Title>
                        {/* {JSON.stringify(ResourceModel)} */}
                    </Modal.Header>
                    <Modal.Body style={{backgroundColor: "white"}}>
                    <div className='form-inside-input'>
                            <div class="col-sm-12">
                            <label className='form-label'> First Name</label>
                            <input type="text" id="firs_name" className="form-control"
                            value={NameModel.first_name} onChange={changeHandler("first_name")}/><br/>
                            <label className='form-label'> Last Name</label>
                            <input type="text" id="last_name" className="form-control" 
                            value={NameModel.last_name} onChange={changeHandler("last_name")}/><br/>
                            </div>
                            </div>  
                    </Modal.Body>
                    <Modal.Footer style={{backgroundColor: "white" }}>
                        <div className="modal-footer">
                        <button style={{backgroundColor:"#1E90FF", borderColor:"#1E90FF"}} onClick={onSave}><font color="white">Update</font></button>
                        <button style={{backgroundColor:'orange', borderColor:'orange'}} onClick={cancelHandle}><font color="white">Cancel</font></button>
                        </div>
                    </Modal.Footer>
                </Modal>  
            </div>
        )
    }
}   
export default FormInput;