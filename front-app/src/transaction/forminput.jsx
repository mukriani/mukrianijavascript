import React from 'react';
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import Select from 'react-select';
import Modal from 'react-bootstrap/Modal';

class FormInput extends React.Component {
    constructor() {
        super();
        this.state = {
            selectedOption: {}
        }
    }
    render() {
        const {TransactionModel, handleCancelDetail,sureReject, cancelHandleDelete, openmodalReject, handleReject,onApproved, selectedHandler, openmodalDetail, UserOption,mode, changeHandler, cancelHandle, cancelHandleCreate,onSave, openmodalEdit, openmodalCreate, handleCancel, errors} = this.props;      
        let options = UserOption.map(function (item) {
            return { value: item.first_name, label: item.first_name }
        });
        return (
            <div>
                 <Modal show={openmodalCreate} style={{opacity:3}} class="modal-lg">
                    <Modal.Header style={{ backgroundColor: "#1E90FF" }} >
                        <Modal.Title style={{backgroundColor: "#1E90FF" }}><h4><font color="white">Add Event Request</font></h4></Modal.Title>
                        {/* {JSON.stringify(ResourceModel)} */}
                    </Modal.Header>
                    <Modal.Body style={{backgroundColor: "white" }}>
                        <div className='form-inside-input'>
                            <div class="col-sm-6">
                            <label className='form-label'> *Transaction Code</label>&nbsp;<input type="text" id="code" className="form-control"
                            value={TransactionModel.code} onChange={changeHandler("code")}/>
                            <label className='form-label'> *Event Name</label>
                            <input type="text" id="event_name" className="form-control" 
                            value={TransactionModel.event_name} onChange={changeHandler("event_name")}/>
                             <span style={{color: "red"}}>{errors["event_name"]}</span><br/>
                            <label className='form-label'> *Event Place</label>
                            <input type="text" id="place" className="form-control"  
                            value={TransactionModel.place} onChange={changeHandler("place")}/>
                             <span style={{color: "red"}}>{errors["place"]}</span><br/>
                            <label className='form-label'>*Event Start Date</label>
                            <input type="date" id="start_date" className="form-control" 
                            value={TransactionModel.start_date} onChange={changeHandler("start_date")}/>
                             <span style={{color: "red"}}>{errors["start_date"]}</span><br/>
                            <label className='form-label'> *Event End Date</label>
                            <input type="date" id="end_date"  className="form-control" 
                            value={TransactionModel.end_date} onChange={changeHandler("end_date")}/>
                             <span style={{color: "red"}}>{errors["end_date"]}</span><br/>
                            <label className='form-label'>*Budget (Rp)</label>
                            <input type="text" id="budget" className="form-control" 
                            value={TransactionModel.budget} onChange={changeHandler("budget")}/>
                              <span style={{color: "red"}}>{errors["budget"]}</span><br/>
                             </div>
                            <div class="col-sm-6">
                            <label className='form-label'> *Request By</label>
                            <input type="text" id="request_by"  className="form-control" 
                            value={TransactionModel.request_by} onChange={changeHandler("request_by")}disabled/><br/>
                            <label className='form-label'>*Request Date</label>
                            <input type="date" id="request_date" className="form-control" 
                            value={TransactionModel.request_date} onChange={changeHandler("request_date")}disabled/><br/>
                            <label className='form-label'> Note</label>
                            <input type="text" id="note"  className="form-control" 
                            value={TransactionModel.note} onChange={changeHandler("note")}/><br/>
                           
                           </div>
                        </div>   
                    </Modal.Body>
                    <Modal.Footer style={{backgroundColor: "white" }}>
                        <div className="modal-footer">
                        <button style={{backgroundColor:"#1E90FF", borderColor:"#1E90FF"}} onClick={onSave}><font color="white">Save</font></button>
                        <button style={{backgroundColor:'orange', borderColor:'orange'}} onClick={cancelHandleCreate}><font color="white">Cancel</font></button>
                        </div>
                    </Modal.Footer>
                </Modal>  
            
                <Modal show={openmodalEdit} style={{opacity:3}}>
                    <Modal.Header style={{ backgroundColor: "#1E90FF" }}>
                        <Modal.Title style={{backgroundColor: "#1E90FF"}}><h4><font color="white">Edit Event Request - {TransactionModel.code}</font></h4></Modal.Title>
                        {/* {JSON.stringify(ResourceModel)} */}
                    </Modal.Header>
                    <Modal.Body style={{backgroundColor: "white"}}>
                    <div className='form-inside-input'>
                            <div class="col-sm-6">
                            <label className='form-label'> *Transaction Code</label>&nbsp;<input type="text" id="code" className="form-control"
                            value={TransactionModel.code} onChange={changeHandler("code")}/>
                            <label className='form-label'> *Event Name</label>
                            <input type="text" id="event_name" className="form-control" 
                            value={TransactionModel.event_name} onChange={changeHandler("event_name")}/>
                             <span style={{color: "red"}}>{errors["event_name"]}</span><br/>
                            <label className='form-label'> *Event Place</label>
                            <input type="text" id="place" className="form-control"  
                            value={TransactionModel.place} onChange={changeHandler("place")}/>
                             <span style={{color: "red"}}>{errors["place"]}</span><br/>
                            <label className='form-label'>*Event Start Date</label>
                            <input type="date" id="start_date" className="form-control" 
                            value={TransactionModel.start_date} onChange={changeHandler("start_date")}/>
                             <span style={{color: "red"}}>{errors["start_date"]}</span><br/>
                            <label className='form-label'> *Event End Date</label>
                            <input type="date" id="end_date"  className="form-control" 
                            value={TransactionModel.end_date} onChange={changeHandler("end_date")}/>
                             <span style={{color: "red"}}>{errors["end_date"]}</span><br/>
                            <label className='form-label'>*Budget (Rp)</label>
                            <input type="text" id="budget" className="form-control" 
                            value={TransactionModel.budget} onChange={changeHandler("budget")}/>
                              <span style={{color: "red"}}>{errors["budget"]}</span><br/>
                             </div>
                            <div class="col-sm-6">
                            <label className='form-label'> *Request By</label>
                            <input type="text" id="request_by"  className="form-control" 
                            value={TransactionModel.request_by} onChange={changeHandler("request_by")}disabled/><br/>
                            <label className='form-label'>*Request Date</label>
                            <input type="date" id="request_date" className="form-control" 
                            value={TransactionModel.request_date} onChange={changeHandler("request_date")}disabled/><br/>
                            <label className='form-label'> Note</label>
                            <input type="text" id="note"  className="form-control" 
                            value={TransactionModel.note} onChange={changeHandler("note")}/><br/>
                            <label className='form-label'> Status</label>
                            <input type="text" id="status"  className="form-control" 
                            value={TransactionModel.status} onChange={changeHandler("status")}disabled/><br/>
                           </div>
                        </div>     
                    </Modal.Body>
                    <Modal.Footer style={{backgroundColor: "white" }}>
                        <div className="modal-footer">
                        <button style={{backgroundColor:"#1E90FF", borderColor:"#1E90FF"}} onClick={onSave}><font color="white">Update</font></button>
                        <button style={{backgroundColor:'orange', borderColor:'orange'}} onClick={cancelHandle}><font color="white">Cancel</font></button>
                        </div>
                    </Modal.Footer>
                </Modal>  

                <Modal show={openmodalDetail} style={{ opacity: 1 }} size='lg'>
                    <Modal.Header style={{ backgroundColor: "#1E90FF" }}>
                        <Modal.Title><h4><font color="white">Approval Event Request - {TransactionModel.code} </font></h4></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <div className='form-inside-input'>
                            <div class="col-sm-6">
                            <label className='form-label'> *Transaction Code</label>&nbsp;<input type="text" id="code" className="form-control"
                            value={TransactionModel.code} onChange={changeHandler("code")}disabled/>
                            <label className='form-label'> *Event Name</label>
                            <input type="text" id="event_name" className="form-control" 
                            value={TransactionModel.event_name} onChange={changeHandler("event_name")}disabled/>
                             <span style={{color: "red"}}>{errors["event_name"]}</span><br/>
                            <label className='form-label'> *Event Place</label>
                            <input type="text" id="place" className="form-control"  
                            value={TransactionModel.place} onChange={changeHandler("place")}disabled/>
                             <span style={{color: "red"}}>{errors["place"]}</span><br/>
                            <label className='form-label'>*Event Start Date</label>
                            <input type="date" id="start_date" className="form-control" 
                            value={TransactionModel.start_date} onChange={changeHandler("start_date")}disabled/>
                             <span style={{color: "red"}}>{errors["start_date"]}</span><br/>
                            <label className='form-label'> *Event End Date</label>
                            <input type="date" id="end_date"  className="form-control" 
                            value={TransactionModel.end_date} onChange={changeHandler("end_date")}disabled/>
                             <span style={{color: "red"}}>{errors["end_date"]}</span><br/>
                            <label className='form-label'>*Budget (Rp)</label>
                            <input type="text" id="budget" className="form-control" 
                            value={TransactionModel.budget} onChange={changeHandler("budget")}disabled/>
                              <span style={{color: "red"}}>{errors["budget"]}</span><br/>
                              <label className='form-label'>*Assign To</label>
                            <Select
                                name="assign_to"
                                value={mode == 'create' ? this.state.value : { label: TransactionModel.first_name, value: TransactionModel.assign_to}}
                                onChange={selectedHandler}
                                options={options}
                                defaultValue={{ label: TransactionModel.first_name, value: TransactionModel.assign_to}}
                                placeholder="-Assign to-"
                            />
                             </div>
                            <div class="col-sm-6">
                            <label className='form-label'> *Request By</label>
                            <input type="text" id="request_by"  className="form-control" 
                            value={TransactionModel.request_by} onChange={changeHandler("request_by")}disabled/><br/>
                            <label className='form-label'>*Request Date</label>
                            <input type="date" id="request_date" className="form-control" 
                            value={TransactionModel.request_date} onChange={changeHandler("request_date")}disabled/><br/>
                            <label className='form-label'> Note</label>
                            <input type="text" id="note"  className="form-control" 
                            value={TransactionModel.note} onChange={changeHandler("note")}disabled/><br/>
                            <label className='form-label'> Status</label>
                            <input type="text" id="status"  className="form-control" 
                            value={TransactionModel.status} onChange={changeHandler("status")}disabled/><br/>
                           
                           </div>
                        </div>  
                    </Modal.Body>
                    <Modal.Footer>
                        <div className="modal-footer">
                        <button type="button" style={{backgroundColor:'orange',color:'white'}} className="btn btn-primary" onClick={onApproved}> Approved </button>
                        <button type="button" style={{backgroundColor:'orange',color:'white'}} className="btn btn-danger" onClick={handleReject}> Rejected </button>
                            <button type="button" style={{backgroundColor:'orange',color:'white'}} className="btn btn-warning" onClick={handleCancelDetail}> Close </button>
                        </div>
                    </Modal.Footer>
                </Modal>

                <Modal show={openmodalReject} style={{ opacity: 1 }}>
                <Modal.Header class="glyphicon glyphicon-remove" >
                <Modal.Title  ></Modal.Title></Modal.Header>
                    <Modal.Body> 
                    <div > <h5><font color ="black">Reject Reason ?</font></h5></div></Modal.Body>
                    <Modal.Footer>
                        <div className="modal-footer">
                        <button style={{backgroundColor:"#1E90FF", borderColor:"blue", color:'white'}} onClick={sureReject}>&nbsp;Rejected&nbsp;</button>
                        <button style={{backgroundColor:'orange', borderColor:'orange', color:'white'}} onClick={cancelHandleDelete}>Cancel</button>
                            
                        </div>
                        </Modal.Footer>
                </Modal>
            </div>
        )
    }
}   
export default FormInput;