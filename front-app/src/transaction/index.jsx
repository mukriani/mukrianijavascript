import React from 'react';
import FormInput from './forminput';
import Table from 'react-bootstrap/Table';
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import Modal from 'react-bootstrap/Modal';
import TransactionService from '../service/TransactionService';
import {ModalBody,ModalFooter,ModalHeader,Button, Container, Row, Col,Card, CardTitle, CardText, Collapse } from 'reactstrap'
import Dropdown from 'react-bootstrap/Dropdown'

class Transaction extends React.Component {
    TransactionModel = {
        id:0,
        code:"",
        event_name:"",
        start_date:"",
        end_date:"",
        place:"",
        budget:"",
        request_by:"",
        request_date:"",
        approved_by:"",
        approved_date:"",
        assign_to:"",
        closed_date:"",
        note:"",
        status:"",
        reject_reason:"",
        is_delete:"",
        created_by:"",
        created_date:"",
        updated_by:"",
        updated_date:""
    }

    constructor() {
        super();
        this.state = {
            ListTransaction: [],
            UserOption: [],
            TransactionModel: this.TransactionModel,
            openmodalEdit: false,
            openmodalReject:false,
            openmodalDetail: false,
            openmodalDelete: false,
            openmodalCreate: false,
            hidden: true,
            errors: {},
            filter: {
                date1:'',
                created:'',
                name1:'',
                code1:'',
                requestby:'',
                requestdate:'',
                orderdate1: '',
                page: '1',
                pagesize: '10'
            },
            totaldata:1
        }
    }

    handleValidation() {
        let fields = this.state.TransactionModel;
        let errors = {};
        let formIsValid = true;

        //Event
        if (!fields["event_name"]) {
            formIsValid = false;
            errors["event_name"] = "Cannot be empty";
        }

        //Tempat
        if (!fields["place"]) {
            formIsValid = false;
            errors["place"] = "Cannot be empty";
        }

        //Tanggal
        if (!fields["start_date"]) {
            formIsValid = false;
            errors["start_date"] = "Cannot be empty";
        }

        //Tanggal
        if (!fields["end_date"]) {
            formIsValid = false;
            errors["end_date"] = "Cannot be empty";
        }

        //Budget
        if (!fields["budget"]) {
            formIsValid = false;
            errors["budget"] = "Cannot be empty";
        }

        //Budget
        if(typeof fields["budget"] !== "undefined"){
            if(!fields["budget"].match(/^[0-9]+$/)){
              formIsValid = false;
              errors["budget"] = "No letters";
            }      	
          }
     
        this.setState({ errors: errors });
        return formIsValid;
    }


    componentDidMount() {
        const {filter} = this.state;
        this.loadList(filter);

    }


    loadList = async (filter) => {
        const respon = await TransactionService.getAlltransaction(filter);
        const countdata = await TransactionService.counttransaction(filter);
        if (respon.success) {
            this.setState({
                ListTransaction: respon.result,
                ListTampil:respon.result,
                totaldata: Math.ceil(countdata.result[0].totaldata / filter.pagesize)
            })
        }
    }

    componentDidUpdate() {
        const {filter}  = this.state;
        this.loadList(filter);
    }


    handleOpen = () => {
        this.getOptionuser();
        this.setState({
            hidden: true,
            openmodalCreate: true,
            TransactionModel: this.TransactionModel,
            mode: 'create'
        })
    }

    cancelHandleCreate = async () => {
        this.setState({
            openmodalCreate: false,
            errors: {}
        });
    }


    cancelHandle = async () => {
        this.setState({
            openmodalEdit: false,
            errors: {}
        });
    }

    handleEdit = async (id) => {
        const respon = await TransactionService.getdatabyid(id);
        if (respon.success) {
            this.setState({
                hidden: false,
                openmodalEdit: true,
                TransactionModel: respon.result,
                mode: "edit"
            })
        }
        else {
            alert('error' + respon.result);
        }
    }


    cancelHandleEdit = async () => {
        // const respon = await BarangService.getAll();
        // if (respon.success) {
        this.setState({
            // BarangModel: respon.result,
            openmodalDelete: false,
            errors: {}
        });

    }

    cancelHandleDelete = async () => {
        this.setState({
            openmodalDelete: false

        });

    }

  

    onSave = async () => {
        const { TransactionModel, filter, mode } = this.state;
          if (this.handleValidation()) {
            if (mode === 'create') {
                const respons = await TransactionService.post(TransactionModel);
                if (respons.success) {
                    alert('Success : ' + respons.result)
                    this.loadList(filter);
                }
                else {
                    alert('Error : ' + respons.result)
                }
                this.setState({
                    hidden: true,
                    openmodalCreate: false
                });
            } else {
                const respon = await TransactionService.updatetransaction(TransactionModel);
                if (respon.success) {
                    alert('Success : ' + respon.result)
                    this.loadList(filter);
                }
                else {
                    alert('Error : ' + respon.result)
                }
                this.setState({
                    hidden: true,
                    openmodalEdit: false
                });
            }
    }else {
        alert("Terjadi kesalahan dalam pengisian form")
    }
}

    selectedHandler = (selectedOption) => {
        const {mode} = this.state;
        if (mode === "create") {
        this.setState({
            TransactionModel: {
                ...this.state.TransactionModel,
                assign_to: selectedOption.value,
                first_name: selectedOption.label
            }
        })
        }else{
        this.setState({
            TransactionModel: {
                ...this.state.TransactionModel,
                assign_to: selectedOption.value,
                first_name: selectedOption.label
                }
            })
        }
    };



    handleDelete = async (id) => {
        const respon = await TransactionService.getdatabyid(id);
        if (respon.success) {
            this.setState({
                hidden:false,
                openmodalDelete: true,
                TransactionModel: respon.result

            });
        }
        else {
            alert('error' + respon.result);
        }
    }

    handleReject = async (id) => {
        const respon = await TransactionService.getdatabyid(id);
        if (respon.success) {
            this.setState({
                hidden:false,
                openmodalReject: true,
                TransactionModel: respon.result

            });
        }
        else {
            alert('error' + respon.result);
        }
    }

    sureReject = async (item) => {
        const {TransactionModel} = this.state;
        const respons = await TransactionService.reject(TransactionModel);
        if (respons.success) {
            alert('succes:' + respons.result)
        }
        else {
            alert('Error : ' + respons.result)
        }
        this.setState({
            openmodalReject: false
        });
    }

    onApproved = async (item) => {
        const {TransactionModel} = this.state;
        const respons = await TransactionService.approve(TransactionModel);
        if (respons.success) {
            alert('succes:' + respons.result)
        }
        else {
            alert('Error : ' + respons.result)
        }
        this.setState({
            openmodalDetail: false
        });
    }


    sureDelete = async (item) => {
        const { TransactionModel, filter } = this.state;
        const respons = await TransactionService.delete(TransactionModel);
        if (respons.success) {
            alert('Success : ' + respons.result)
            this.loadList(filter);
        }
        else {
            alert('Error : ' + respons.result)
        }
        this.setState({
            openmodalDelete: false
        });
    }

    changeHandler = name => ({ target: { value } }) => {
        this.setState({
            TransactionModel: {
                ...this.state.TransactionModel,
                [name]: value,
            }
        })
    }

    

  
    handleDetail = async (id) => {
        const respon = await TransactionService.getdetailbyid(id);
        this.getOptionuser();
        console.log(respon)
        if (respon.success) {
            this.setState({
                hidden:false,
                TransactionModel:respon.result,
                openmodalDetail: true,
            })
        }
    else {
        alert('error' + respon.result);
    }
}

    handleCancelDetail = async () => {
        this.setState({
            openmodalDetail: false
        })
    }

    getOptionuser = async () => {
        const respons = await TransactionService.getuser();
        console.log(respons)
        this.setState({
            UserOption: respons.result
        });
        console.log(this.state.UserOption)
    }

    filterHandler = val => ({ target: { value } }) => {
        this.setState({
            filter: {
                ...this.state.filter,
                [val]: value
            }
        })
    }

    onChangePage = (number) => {
        const { filter } = this.state;
        this.setState({
            filter: {
                ...this.state.filter,
                ["page"]: number
            }
        });
    }

    handlerSorting = () => {
        let orderdate1 = "";
        const { filter } = this.state;
        if (filter.orderdate1 === "") {
            orderdate1 = "DESC"
        }
        this.setState({
            filter: {
                ...this.state.filter,
                ["orderdate1"]: orderdate1
            }
        })
    }


    renderPagination() {
        let items = [];
        const { filter, totaldata } = this.state;
        for (let number = 1; number <= totaldata; number++) {
            items.push(
                <PaginationItem key={number} active={number === filter.page}>
                    <PaginationLink onClick={() => this.onChangePage(number)} next>
                        {number}
                    </PaginationLink>
                </PaginationItem>
            );
        }
        return (
            <Pagination>{items}</Pagination>
        );
    }

    pageSizeHandler = (val) => {
        this.setState({
            filter: {
                ...this.state.filter,
                ["pagesize"]: val
            }
        });
    }


    render() {
        const { ListTransaction, TransactionModel, UserOption, hidden, openmodalEdit,openmodalReject, openmodalCreate, filter, errors, mode, openmodalDelete, handleCancelDetail,
            handleDetail, openmodalDetail } = this.state;
        return (

            <div>                
                <FormInput
                    ListTransaction={ListTransaction}
                    errors={errors}
                    openmodalReject={openmodalReject}
                    openmodalDetail={openmodalCreate}
                    openmodalCreate={openmodalCreate}
                    openmodalDetail={openmodalDetail}
                   
                    TransactionModel={TransactionModel}
                    hidden={hidden}
                    title={mode}
                    handleDetail={this.handleDetail}
                    handleCancelDetail={this.handleCancelDetail}
                    openmodalEdit={openmodalEdit}
                    handleOpen={this.handleOpen}
                    handleReject={this.handleReject}
                    openmodalDelete={openmodalDelete}
                    selectedHandler={this.selectedHandler}
                    mode={mode}
                    changeHandler={this.changeHandler}
                    cancelHandleCreate={this.cancelHandleCreate}
                    cancelHandle={this.cancelHandle}
                    onSave={this.onSave}
                    onApproved={this.onApproved}
                    UserOption={UserOption}
                    sureDelete={this.sureDelete}
                />
               <div >
                     <div style={{backgroundColor:"#1E90FF", fontSize:'15px'}} class="card-body"><font color="white">List Event Request</font></div>
                 </div><br/>
                 <CardText style={{color:'#1E90FF', fontStyle:'Roboto'}}>
                 <h5>
                    <p>
                        <a href="#">Home</a>
                        <a>&nbsp;/&nbsp;</a>
                        <a href="#">Master</a>
                        <a>&nbsp;/&nbsp;</a>
                        <a href="#">List Event Request</a>
                        </p>
                    <button type="button" style={{float:'right'}} class="btn btn-primary btn-lg" onClick={() => this.handleOpen()}>Add</button><br/><br/><br/>
                    <form style={{float:'right'}} class="form-inline">
                    <input class="form-control-sm" type="text" id="code1" placeholder="Transaction Code" onChange={this.filterHandler("code1")} />&emsp;
                    <input class="form-control-sm" type="text" id="requestby" placeholder="Request By" onChange={this.filterHandler("requestby")} />&emsp;
                    <input class="form-control-sm" type="date" id="requestdate" placeholder="Request Date" onChange={this.filterHandler("requestdate")} />&emsp;
                    <input class="form-control-sm" type="text" id="name1" placeholder ="Status" onChange={this.filterHandler("name1")} />&emsp;
                    <input class="form-control-sm" type="date" id="date1" placeholder="Created" onChange={this.filterHandler("date1")} />&emsp;
                    <input class="form-control-sm" type="text" id="created" placeholder ="Created By" onChange={this.filterHandler("created")} />&emsp;
                    <button type="button" style={{float:'right'}} class="btn btn-warning btn-lg"><font color="white">Search</font></button>
                    </form>
                <table class='table table-striped table-xl table-hover' >
                    <thead>
                        <tr style={{borderBottomStyle:'ridge',border:'1',borderBottomColor:'#000066',borderBottomWidth:'2px',textAlign:'-moz-ridge',width:'90%'}}>
                            <th>NO</th>
                            <th>Transaction Code</th>
                            <th>Request By</th>
                            <th>Request Date</th>
                            <th>Status</th>
                            <th>Create Date</th>
                            <th>Created By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            ListTransaction.map(co => {
                                return (
                                    <tr  >
                                        <td>{co.id}</td>
                                        <td>{co.code}</td>
                                        <td>{co.request_by}</td>
                                        <td>{co.request_date}</td>
                                        <td>{co.status}</td>
                                        <td>{co.created_date}</td>
                                        <td>{co.created_by}</td>
                                        <td>
                                         <span class="glyphicon glyphicon-search" onClick={() => this.handleDetail(co.id)}></span>&nbsp;
                                         <span class="glyphicon glyphicon-pencil" onClick={() => this.handleEdit(co.id)}></span>&nbsp;
                                        
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                {this.renderPagination()}
                <Modal show={openmodalDelete} style={{ opacity: 1 }}>
                <Modal.Header class="glyphicon glyphicon-remove" >
                <Modal.Title  ></Modal.Title></Modal.Header>
                    <Modal.Body> 
                    <div > <h5><font color ="black">Delete Data ?</font></h5></div></Modal.Body>
                    <Modal.Footer>
                        <div className="modal-footer">
                        <button style={{backgroundColor:"#1E90FF", borderColor:"blue", color:'white'}} onClick={() => this.sureDelete()}>&nbsp;Delete&nbsp;</button>
                        <button style={{backgroundColor:'orange', borderColor:'orange', color:'white'}} onClick={() => this.cancelHandleDelete()}>Cancel</button>
                            
                        </div>
                        </Modal.Footer>
                </Modal>
                

{/* 
                <Modal show={openmodalDetail} style={{ opacity: 1 }} size='lg'>
                    <Modal.Header style={{ backgroundColor: "#1E90FF" }}>
                        <Modal.Title><h4><font color="white">Approval Event Request - {TransactionModel.code} </font></h4></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    
<body>
      <div class="col-sm-6">
    <label style={{color:'black',fontSize:'11px'}} className='form-label'> *Transaction Code </label><br />
    <label style={{backgroundColor:"#D3D3D3",height:'100%', width:'100%'}}><h5><b><font color='black'>{TransactionModel.code}</font></b></h5> </label><br />
    <label style={{color:'black',fontSize:'11px'}} className='form-label'> *Event Name</label><br />
    <label><h5><b><font color='black'>{TransactionModel.event_name}</font></b></h5></label><br />
    <label style={{color:'black',fontSize:'11px'}} className='form-label'> *Event Place</label><br />
    <label><h5><b><font color='black'>{TransactionModel.place}</font></b></h5> </label><br />
    </div>
    <div class="col-sm-6">
    <label style={{color:'black',fontSize:'11px'}} className='form-label'> *Event Start Date</label><br />
    <label><h5><b><font color='black'>{TransactionModel.start_date}</font></b></h5> </label><br />
    <label style={{color:'black',fontSize:'11px'}} className='form-label'>*Event End Date</label><br />
    <label><h5><b><font color='black'>{TransactionModel.end_date}</font></b></h5> </label><br />
    <label style={{color:'black',fontSize:'11px'}} className='form-label'> *Budget (Rp.)</label><br />
    <label><h5><b><font color='black'>{TransactionModel.budget}</font></b></h5> </label><br />
    <label style={{color:'black',fontSize:'11px'}} className='form-label'>*Assign To</label><br />
    <label><h5><b><font color='black'>{TransactionModel.assign_to}</font></b></h5> </label><br />
    <label style={{color:'black',fontSize:'11px'}} className='form-label'> *Request By</label><br />
    <label><h5><b><font color='black'>{TransactionModel.request_by}</font></b></h5> </label><br />
    <label style={{color:'black',fontSize:'11px'}} className='form-label'>*Request Date</label><br />
    <label><h5><b><font color='black'>{TransactionModel.request_date}</font></b></h5> </label><br />
    <label style={{color:'black',fontSize:'11px'}} className='form-label'> Note</label><br />
    <label><h5><b><font color='black'>{TransactionModel.note}</font></b></h5> </label><br />
    <label style={{color:'black',fontSize:'11px'}} className='form-label'>Status</label><br />
    <label><h5><b><font color='black'>{TransactionModel.status}</font></b></h5> </label><br />
    </div>
</body>

                    </Modal.Body>
                    <Modal.Footer>
                        <div className="modal-footer">
                            <button type="button" style={{backgroundColor:'orange',color:'white'}} className="btn btn-warning" onClick={() => this.handleCancelDetail()}> Close </button>
                        </div>
                    </Modal.Footer>
                </Modal> */}
</h5>
                </CardText>

            </div>
        )
    }
}

export default Transaction;


