import React from 'react';
import FormInput from './forminput';
import Table from 'react-bootstrap/Table';
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import Modal from 'react-bootstrap/Modal';
import CompanyService from '../service/CompanyService';
import {ModalBody,ModalFooter,ModalHeader,Button, Container, Row, Col,Card, CardTitle, CardText, Collapse } from 'reactstrap'
import Dropdown from 'react-bootstrap/Dropdown'

class Company extends React.Component {
    CompanyModel = {
        id:0,
        code:"",
        name:"",
        address:"",
        phone:"",
        email:"",
        is_delete:false,
             
    }

    constructor() {
        super();
        this.state = {
            ListCompany: [],
            CompanyModel: this.CompanyModel,
            openmodalEdit: false,
            openmodalDetail: false,
            openmodalDelete: false,
            openmodalCreate: false,
            hidden: true,
            errors: {},
            filter: {
                date1:'',
                created:'',
                name1:'',
                code1:'',
                orderdate1: '',
                page: '1',
                pagesize: '10'
            },
            totaldata:1
        }
    }

    handleValidation() {
        let fields = this.state.CompanyModel;
        let errors = {};
        let formIsValid = true;

        //Name
        if (!fields["name"]) {
            formIsValid = false;
            errors["name"] = "Cannot be empty";
        }

        //Email
        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "Cannot be empty";
        } else if (typeof fields['email'] !== "undefined") {
            let lastAtPos = fields['email'].lastIndexOf('@');
            let lastDotPos = fields['email'].lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields['email'].indexOf('@@') == -1
                && lastDotPos > 2 && (fields['email'].length - lastDotPos) > 2)) {
                formIsValid = false;
                errors['email'] = 'Email is not valid';
            }
        }


        //Phone
        if(typeof fields["phone"] !== "undefined"){
            if(!fields["phone"].match(/^[0-9+-]+$/)){
              formIsValid = false;
              errors["phone"] = "Not letters";
            }      	
          }
     
        this.setState({ errors: errors });
        return formIsValid;
    }


    componentDidMount() {
        const {filter} = this.state;
        this.loadList(filter);

    }


    loadList = async (filter) => {
        const respon = await CompanyService.getAllcompany(filter);
        const countdata = await CompanyService.countcompany(filter);
        if (respon.success) {
            this.setState({
                ListCompany: respon.result,
                ListTampil:respon.result,
                totaldata: Math.ceil(countdata.result[0].totaldata / filter.pagesize)
            })
        }
    }

    componentDidUpdate() {
        const {filter}  = this.state;
        this.loadList(filter);
    }


    handleOpen = () => {
        this.setState({
            hidden: true,
            openmodalCreate: true,
            CompanyModel: this.CompanyModel,
            mode: 'create'
        })
    }

    cancelHandleCreate = async () => {
        this.setState({
            openmodalCreate: false,
            errors: {}
        });
    }


    cancelHandle = async () => {
        this.setState({
            openmodalEdit: false,
            errors: {}
        });
    }

    handleEdit = async (id) => {
        const respon = await CompanyService.getdatabyid(id);
        if (respon.success) {
            this.setState({
                hidden: false,
                openmodalEdit: true,
                CompanyModel: respon.result,
                mode: "edit"
            })
        }
        else {
            alert('error' + respon.result);
        }
    }


    cancelHandleEdit = async () => {
        // const respon = await BarangService.getAll();
        // if (respon.success) {
        this.setState({
            // BarangModel: respon.result,
            openmodalDelete: false,
            errors: {}

        });

    }

    cancelHandleDelete = async () => {
        this.setState({
            openmodalDelete: false

        });

    }

  

    onSave = async () => {
        const { CompanyModel, filter, mode } = this.state;
        if (this.handleValidation()) {
            if (mode === 'create') {
                const respons = await CompanyService.post(CompanyModel);
                if (respons.success) {
                    alert('Success : ' + respons.result)
                    this.loadList(filter);
                }
                else {
                    alert('Error : ' + respons.result)
                }
                this.setState({
                    hidden: true,
                    openmodalCreate: false
                });
            } else {
                const respon = await CompanyService.updatecompany(CompanyModel);
                if (respon.success) {
                    alert('Success : ' + respon.result)
                    this.loadList(filter);
                }
                else {
                    alert('Error : ' + respon.result)
                }
                this.setState({
                    hidden: true,
                    openmodalEdit: false
                });
            }
        } else {
            alert("Terjadi kesalahan dalam pengisian form")
        }
    }



    handleDelete = async (id) => {
        const respon = await CompanyService.getdatabyid(id);
        if (respon.success) {
            this.setState({
                hidden:false,
                openmodalDelete: true,
                CompanyModel: respon.result

            });
        }
        else {
            alert('error' + respon.result);
        }
    }

    sureDelete = async (item) => {
        const { CompanyModel, filter } = this.state;
        const respons = await CompanyService.delete(CompanyModel);
        if (respons.success) {
            alert('Success : ' + respons.result)
            this.loadList(filter);
        }
        else {
            alert('Error : ' + respons.result)
        }
        this.setState({
            openmodalDelete: false
        });
    }

    changeHandler = name => ({ target: { value } }) => {
        this.setState({
        CompanyModel: {
                ...this.state.CompanyModel,
                [name]: value,
            }
        })
    }

    

    // selectedHandler = (selectedOption) => {
    //     const {mode} = this.state;
    //     if (mode === "create") {
    //     this.setState({
    //         CompanyModel: {
    //             ...this.state.CompanyModel,
    //             client_id: selectedOption.value,
    //             name: selectedOption.label
    //         }
    //     })
    //     }else{
    //     this.setState({
    //         ResourceModel: {
    //             ...this.state.ResourceModel,
    //             client_id: selectedOption.value,
    //             name: selectedOption.label
    //             }
    //         })
    //     }
    // };

    handleDetail = async (id) => {
        const respon = await CompanyService.getdetailbyid(id);
        console.log(respon)
        if (respon.success) {
            this.setState({
                hidden:false,
                CompanyModel:respon.result,
                openmodalDetail: true,
            })
        }
    else {
        alert('error' + respon.result);
    }
}

    handleCancelDetail = async () => {
        this.setState({
            openmodalDetail: false
        })
    }

    // getOptionclient = async () => {
    //     const respons = await ClientService.getclient();
    //     console.log(respons)
    //     this.setState({
    //         ClientOption: respons.result
    //     });
    //     console.log(this.state.ClientOption)
    // }

    filterHandler = val => ({ target: { value } }) => {
        this.setState({
            filter: {
                ...this.state.filter,
                [val]: value
            }
        })
    }

    onChangePage = (number) => {
        const { filter } = this.state;
        this.setState({
            filter: {
                ...this.state.filter,
                ["page"]: number
            }
        });
    }

    handlerSorting = () => {
        let orderdate1 = "";
        const { filter } = this.state;
        if (filter.orderdate1 === "") {
            orderdate1 = "DESC"
        }
        this.setState({
            filter: {
                ...this.state.filter,
                ["orderdate1"]: orderdate1
            }
        })
    }


    renderPagination() {
        let items = [];
        const { filter, totaldata } = this.state;
        for (let number = 1; number <= totaldata; number++) {
            items.push(
                <PaginationItem key={number} active={number === filter.page}>
                    <PaginationLink onClick={() => this.onChangePage(number)} next>
                        {number}
                    </PaginationLink>
                </PaginationItem>
            );
        }
        return (
            <Pagination>{items}</Pagination>
        );
    }

    pageSizeHandler = (val) => {
        this.setState({
            filter: {
                ...this.state.filter,
                ["pagesize"]: val
            }
        });
    }


    render() {
        const { ListCompany, CompanyModel, hidden, openmodalEdit, openmodalCreate, filter, errors, mode, openmodalDelete, handleCancelDetail,
            handleDetail, openmodalDetail } = this.state;
        return (

            <div>                
                <FormInput
                    ListCompany={ListCompany}
                    errors={errors}
                    openmodalCreate={openmodalCreate}
                    openmodalDetail={openmodalDetail}
                    handleDetail={this.handleDetail}
                    CompanyModel={CompanyModel}
                    hidden={hidden}
                    title={mode}
                    handleCancelDetail={this.handleCancelDetail}
                    openmodalEdit={openmodalEdit}
                    handleOpen={this.handleOpen}
                    openmodalDelete={openmodalDelete}
                    selectedHandler={this.selectedHandler}
                    mode={mode}
                    onSelect={this.onSelect}
                    changeHandler={this.changeHandler}
                    cancelHandleCreate={this.cancelHandleCreate}
                    cancelHandle={this.cancelHandle}
                    onSave={this.onSave}
                    sureDelete={this.sureDelete}
                />
                 <div >
                     <div style={{backgroundColor:"#1E90FF", fontSize:'15px'}} class="card-body"><font color="white">List Company</font></div>
                 </div><br/>
                 <CardText style={{color:'#1E90FF', fontStyle:'Roboto'}}>
                 <h5>
                    <p>
                        <a href="#">Home</a>
                        <a>&nbsp;/&nbsp;</a>
                        <a href="#">Master</a>
                        <a>&nbsp;/&nbsp;</a>
                        <a href="#">List Company</a>
                        </p>
                        
                <button type="button" style={{float:'right'}} class="btn btn-primary btn-lg" onClick={() => this.handleOpen()}>Add</button><br/><br/><br/>
                    <form style={{float:'right'}} class="form-inline">
                    <input class="form-control-lg" type="text" id="code1" placeholder="-Select Company Code-" onChange={this.filterHandler("code1")} />&emsp;
                        <input class="form-control-lg" type="text" id="name1" placeholder ="-Select Company Name-" onChange={this.filterHandler("name1")} />&emsp;
                        <input class="form-control-lg" type="date" id="date1" placeholder="Created" onChange={this.filterHandler("date1")} />&emsp;
                        <input class="form-control-lg" type="text" id="created" placeholder ="Created By" onChange={this.filterHandler("created")} />&emsp;
                        <button type="button" style={{Align:'right'}} class="btn btn-warning btn-lg"><font color="white">Search</font></button>
                    </form>
               
                <table class='table table-striped table-xl table-hover' >
                    <thead>
                        <tr style={{borderBottomStyle:'ridge',border:'1',borderBottomColor:'#000066',borderBottomWidth:'2px',textAlign:'-moz-ridge',width:'90%'}}>
                            <th>NO</th>
                            <th>Company Code</th>
                            <th>Company Name</th>
                            <th>Created Date</th>
                            <th>Created By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            ListCompany.map(co => {
                                return (
                                    <tr  >
                                        <td>{co.id}</td>
                                        <td>{co.code}</td>
                                        <td>{co.name}</td>
                                        <td>{co.created_date}</td>
                                        <td>{co.created_by}</td>
                                        <td>
                                                    <span class="glyphicon glyphicon-search" onClick={() => this.handleDetail(co.id)}></span>&nbsp;
                                                    <span class="glyphicon glyphicon-pencil" onClick={() => this.handleEdit(co.id)}></span>&nbsp;
                                                    <span class="glyphicon glyphicon-trash" onClick={() => this.handleDelete(co.id)}></span>
                                        
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                {this.renderPagination()}
                <Modal show={openmodalDelete} style={{ opacity: 1 }}>
                <Modal.Header class="glyphicon glyphicon-remove" >
                <Modal.Title  ></Modal.Title></Modal.Header>
                    <Modal.Body> 
                    <div > <h5><font color ="black">Delete Data ?</font></h5></div></Modal.Body>
                    <Modal.Footer>
                        <div className="modal-footer">
                        <button style={{backgroundColor:"#1E90FF", borderColor:"blue", color:'white'}} onClick={() => this.sureDelete()}>&nbsp;Delete&nbsp;</button>
                        <button style={{backgroundColor:'orange', borderColor:'orange', color:'white'}} onClick={() => this.cancelHandleDelete()}>Cancel</button>
                            
                        </div>
                        </Modal.Footer>
                </Modal>
                


                <Modal show={openmodalDetail} style={{ opacity: 1 }} size='lg'>
                    <Modal.Header style={{ backgroundColor: "#1E90FF" }}>
                        <Modal.Title><h4><font color="white">View Company - PT. Xsis Mitra Utama ({CompanyModel.code}) </font></h4></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    
<body>
      <div class="col-sm-6">
    <label style={{color:'black',fontSize:'11px'}} className='form-label'> *Company Code </label><br />
    <label><h5><b><font color='black'>{CompanyModel.code}</font></b></h5> </label><br />
    <label style={{color:'black',fontSize:'11px'}} className='form-label'> Email</label><br />
    <label><h5><b><font color='black'>{CompanyModel.email}</font></b></h5></label><br />
    <label style={{color:'black',fontSize:'11px'}} className='form-label'> Phone</label><br />
    <label><h5><b><font color='black'>{CompanyModel.phone}</font></b></h5> </label><br />
    </div>
    <div class="col-sm-6">
    <label style={{color:'black',fontSize:'11px'}} className='form-label'> *Company Name</label><br />
    <label><h5><b><font color='black'>{CompanyModel.name}</font></b></h5> </label><br />
    <label style={{color:'black',fontSize:'11px'}} className='form-label'>Address </label><br />
    <label><h5><b><font color='black'>{CompanyModel.address}</font></b></h5> </label><br />
    </div>
</body>

                    </Modal.Body>
                    <Modal.Footer>
                        <div className="modal-footer">
                            <button type="button" style={{backgroundColor:'orange',color:'white'}} className="btn btn-warning" onClick={() => this.handleCancelDetail()}> Close </button>
                        </div>
                    </Modal.Footer>
                </Modal>
                </h5>

                
                </CardText>

            </div>
        )
    }
}

export default Company;


