import React from 'react';
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import Select from 'react-select';
import Modal from 'react-bootstrap/Modal';

class FormInput extends React.Component {
    constructor() {
        super();
        this.state = {
            selectedOption: {}
        }
    }
    render() {
        const {CompanyModel, selectedHandler, mode, changeHandler, cancelHandle, cancelHandleCreate,onSave, openmodalEdit, openmodalCreate, handleCancel, errors} = this.props;      
        return (
            <div>
                 <Modal show={openmodalCreate} style={{opacity:3}} class="modal-lg">
                    <Modal.Header style={{ backgroundColor: "#1E90FF" }} >
                        <Modal.Title style={{backgroundColor: "#1E90FF" }}><h4><font color="white">Add Company</font></h4></Modal.Title>
                        {/* {JSON.stringify(ResourceModel)} */}
                    </Modal.Header>
                    <Modal.Body style={{backgroundColor: "white" }}>
                        <div className='form-inside-input'>
                            <div class="col-sm-6">
                            <label className='form-label'> *Company Code</label>&nbsp;<input type="text" id="code" className="form-control"
                            value={CompanyModel.code} onChange={changeHandler("code")}/><br/>
                            <label className='form-label'> Email</label>
                            <input type="text" id="email" className="form-control" 
                            value={CompanyModel.email} onChange={changeHandler("email")}/><br/>
                            <span style={{color: "red"}}>{errors["email"]}</span>
                            <label className='form-label'> Phone</label>
                            <input type="text" id="phone" className="form-control"  
                            value={CompanyModel.phone} onChange={changeHandler("phone")}/>
                             <span style={{color: "red"}}>{errors["phone"]}</span><br/>
                            </div>
                            <div class="col-sm-6">
                            <label className='form-label'>*Company Name</label>
                            <input type="text" id="name" className="form-control" 
                            value={CompanyModel.name} onChange={changeHandler("name")}/><br/>
                            <span style={{color: "red"}}>{errors["name"]}</span>
                            <label className='form-label'> Address</label>
                            <input type="text" id="address"  className="form-control" 
                            value={CompanyModel.address} onChange={changeHandler("address")}/><br/>
                           </div>
                        </div>   
                    </Modal.Body>
                    <Modal.Footer style={{backgroundColor: "white" }}>
                        <div className="modal-footer">
                        <button style={{backgroundColor:"#1E90FF", borderColor:"#1E90FF"}} onClick={onSave}><font color="white">Save</font></button>
                        <button style={{backgroundColor:'orange', borderColor:'orange'}} onClick={cancelHandleCreate}><font color="white">Cancel</font></button>
                        </div>
                    </Modal.Footer>
                </Modal>  
            
                <Modal show={openmodalEdit} style={{opacity:3}}>
                    <Modal.Header style={{ backgroundColor: "#1E90FF" }}>
                        <Modal.Title style={{backgroundColor: "#1E90FF"}}><h4><font color="white">Edit Company - PT. Xsis Mitra Utama ({CompanyModel.code})</font></h4></Modal.Title>
                        {/* {JSON.stringify(ResourceModel)} */}
                    </Modal.Header>
                    <Modal.Body style={{backgroundColor: "white"}}>
                        <div className='form-inside-input'>
                            <div class="col-sm-6">
                            <label className='form-label'> *Company Code</label>&nbsp;                            <input type="text" id="code" className="form-control"
                            value={CompanyModel.code} onChange={changeHandler("code")}/><br/>
                            <label className='form-label'> Email</label><br/>
                            <input type="text" id="email" className="form-control" 
                            value={CompanyModel.email} onChange={changeHandler("email")}/><br/>
                            <span style={{color: "red"}}>{errors["email"]}</span>
                            <label className='form-label'> Phone</label><br/>
                            <input type="text" id="phone" className="form-control"  
                            value={CompanyModel.phone} onChange={changeHandler("phone")}/>
                             <span style={{color: "red"}}>{errors["phone"]}</span><br/>
                            </div>
                            <div class="col-sm-6">
                            <label className='form-label'>*Company Name</label><br/>
                            <input type="text" id="name" className="form-control" 
                            value={CompanyModel.name} onChange={changeHandler("name")}/><br/>
                            <span style={{color: "red"}}>{errors["name"]}</span>
                            <label className='form-label'> Address</label><br/>
                            <input type="text" id="address"  className="form-control" 
                            value={CompanyModel.address} onChange={changeHandler("address")}/><br/>
                           </div>
                        </div>   
                    </Modal.Body>
                    <Modal.Footer style={{backgroundColor: "white" }}>
                        <div className="modal-footer">
                        <button style={{backgroundColor:"#1E90FF", borderColor:"#1E90FF"}} onClick={onSave}><font color="white">Update</font></button>
                        <button style={{backgroundColor:'orange', borderColor:'orange'}} onClick={cancelHandle}><font color="white">Cancel</font></button>
                        </div>
                    </Modal.Footer>
                </Modal>  
            </div>
        )
    }
}   
export default FormInput;